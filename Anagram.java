//{ Driver Code Starts
import java.util.*;
import java.util.stream.*; 
import java.lang.*;
import java.io.*;

class GFG {
    
	public static void main (String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		while(t-->0)
		{
		    String str = br.readLine();
		    String s1 = str.split(" ")[0];
		    String s2 = str.split(" ")[1];
		    
		    Solution obj = new Solution();
		    
		    if(obj.isAnagram(s1,s2))
		    {
		        System.out.println("YES");
		    }
		    else
		    {
		         System.out.println("NO");
		    }
		    
		    
		    
		}
	}
}
// } Driver Code Ends


class Solution {
    public static boolean isAnagram(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        
        Map<Character, Integer> count = new HashMap<>();
        
        // Increment count for characters in string a
        for (char c : a.toCharArray()) {
            count.put(c, count.getOrDefault(c, 0) + 1);
        }
        
        // Decrement count for characters in string b
        for (char c : b.toCharArray()) {
            int updatedCount = count.getOrDefault(c, 0) - 1;
            if (updatedCount < 0) {
                return false;
            }
            count.put(c, updatedCount);
        }
        
        // If all counts are zero, return true
        return true;
    }
}
