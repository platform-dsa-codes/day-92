class Solution {
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        
        int[] count = new int[26]; // Assuming lowercase English letters
        
        // Increment count for characters in string s
        for (char c : s.toCharArray()) {
            count[c - 'a']++;
        }
        
        // Decrement count for characters in string t
        for (char c : t.toCharArray()) {
            count[c - 'a']--;
        }
        
        // If count array has any non-zero values, return false
        for (int i = 0; i < 26; i++) {
            if (count[i] != 0) {
                return false;
            }
        }
        
        // If count array has all zeros, return true
        return true;
    }
}
